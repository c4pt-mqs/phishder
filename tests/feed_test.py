import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import Session, declarative_base, sessionmaker

app = FastAPI()

test_feed = {
    "id": 1,
    "url": "https://example.com"
}

TEST_DATABASE_URL = "postgresql://postgres:postgres@localhost:5432/test_feed_db"
Base = declarative_base()

@pytest.fixture
def db():
    engine = create_engine(TEST_DATABASE_URL)
    TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    db = TestingSessionLocal()
    Base.metadata.create_all(bind=engine)
    try:
        yield db
    finally:
        db.close()

class TestFeed(Base):
    __tablename__ = "test_feed"
    id = Column(Integer, primary_key=True)
    url = Column(String)

def add_test_feed(id: int, url: str, db: Session):
    feed = TestFeed(id=id, url=url)
    db.add(feed)
    db.commit()
    db.refresh(feed)
    return feed

def test_root_dir():
    client = TestClient(app)
    response = client.get("/")
    response.status_code == 200
    response.json() == {"message": "Server is running!"}

def crawl_feeds(db: Session):
    for data in [test_feed]:
        id = data['id']
        url = data['url']
        add_test_feed(id=id, url=url, db=db)

    print("Feeds updated successfully.")

@pytest.mark.asyncio
async def test_crawl_feeds(db: Session):
    await crawl_feeds(db)
    test_feeds = db.query(TestFeed).all()
    assert len(test_feeds) > 0
    assert test_feeds[0].id == test_feed["id"]
    assert test_feeds[0].url == test_feed["url"]
