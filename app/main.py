import uvicorn
import threading
from views.feed_api import app
from controllers.feed_controller import feed_controller
from config import settings

if __name__ == "__main__":
    crawl_thread = threading.Thread(target=feed_controller.crawl_feeds)
    crawl_thread.start()
    uvicorn.run(app, host=settings.HOST, port=settings.PORT)
