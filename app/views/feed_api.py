from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session
from models.feed_db import get_db, PhishTankFeed, OpenPhishFeed, PhishStatsFeed
from models.schemas import PhishTankFeedSchema, OpenPhishFeedSchema, PhishStatsFeedSchema

app = FastAPI()

@app.get("/")
def root_dir():
    return {"message": "Server is running!"}

@app.get("/phishtank_feed")
def get_phishtank_feed(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    feeds = db.query(PhishTankFeed).offset(skip).limit(limit).all()
    return [PhishTankFeedSchema(id=feed.id, url=feed.url) for feed in feeds]

@app.get("/openphish_feed")
def get_openphish_feed(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    feeds = db.query(OpenPhishFeed).offset(skip).limit(limit).all()
    return [OpenPhishFeedSchema(url=feed.url, target_brand=feed.tar_brand, time=feed.time) for feed in feeds]

@app.get("/phishstats_feed")
def get_phishstats_feed(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    feeds = db.query(PhishStatsFeed).offset(skip).limit(limit).all()
    return [PhishStatsFeedSchema(id=feed.id, url=feed.url, ip=feed.ip) for feed in feeds]
