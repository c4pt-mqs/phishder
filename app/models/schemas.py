from pydantic import BaseModel

class PhishTankFeedSchema(BaseModel):
    id: int
    url: str

class OpenPhishFeedSchema(BaseModel):
    url: str
    target_brand: str
    time: str

class PhishStatsFeedSchema(BaseModel):
    id: int
    url: str
    ip: str
