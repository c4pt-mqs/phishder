from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from config import settings

engine = create_engine(settings.DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

# Generator Function | Generate the data step by step at any time
def get_db() -> Session:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# Create tables in Base
class Feed(Base):
    __abstract__ = True

    def add_feed(self, db: Session, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
        db.add(self)

    def update_feed(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

class PhishTankFeed(Feed):
    __tablename__ = "phishtank_feed"

    id = Column(Integer, primary_key=True)
    url = Column(String)

class OpenPhishFeed(Feed):
    __tablename__ = "openphish_feed"

    url = Column(String, primary_key=True)
    tar_brand = Column(String)
    time = Column(String)

class PhishStatsFeed(Feed):
    __tablename__ = "phishstats_feed"

    id = Column(Integer, primary_key=True)
    url = Column(String)
    ip = Column(String)

# Create all of the tables
Base.metadata.create_all(bind=engine)