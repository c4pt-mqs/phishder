import logging
from threading import Timer
from feeds.phishtank import get_phishtank_feed
from feeds.openphish import get_openphish_feed
from feeds.phishstats import get_phishstats_feed
from models.feed_db import get_db, PhishTankFeed, OpenPhishFeed, PhishStatsFeed
from models.schemas import PhishTankFeedSchema, OpenPhishFeedSchema, PhishStatsFeedSchema

class FeedController:
    def __init__(self):
        self.timer_interval = 10
        self.db = next(get_db())

    def crawl_feeds(self):
        try:
            # PhishTank
            for data in get_phishtank_feed():
                feed_data = PhishTankFeedSchema(id=data['phish_id'], url=data['url'])
                self.add_feed(PhishTankFeed, url=feed_data.url)

            # OpenPhish
            for data in get_openphish_feed():
                feed_data = OpenPhishFeedSchema(url=data['url'], target_brand=data['target_brand'], time=data['time'])
                self.add_feed(OpenPhishFeed, url=feed_data.url, tar_brand=feed_data.target_brand, time=feed_data.time)

            # PhishStats
            for data in get_phishstats_feed():
                feed_data = PhishStatsFeedSchema(id=data['id'], url=data['url'], ip=data['ip'])
                self.add_feed(PhishStatsFeed, url=feed_data.url, ip=feed_data.ip)

            logging.info("Feeds updated successfully.")

        except Exception as e:
            logging.error(f"Error while crawling feeds: {e}")

        # Scan every 10 seconds
        Timer(self.timer_interval, self.crawl_feeds).start()

    def add_feed(self, feed_class, **kwargs):
        feed = self.db.query(feed_class).filter_by(**kwargs).first()

        if feed:
            # If the feed already exists, update it
            feed.update_feed(**kwargs)
        else:
            # If the feed doesn't exist, insert a new record
            new_feed = feed_class()
            new_feed.add_feed(self.db, **kwargs)

        self.db.commit()

# Create an instance of FeedController to be used in the application
feed_controller = FeedController()