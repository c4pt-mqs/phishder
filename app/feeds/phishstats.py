import requests
from config import settings

url = settings.PHISHSTATS_URL

def get_phishstats_feed():
    response = requests.get(url, timeout=10)
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        print(f"Error while fetching PhishStats feed. Status code: {response.status_code}")
        return []
