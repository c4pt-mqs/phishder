import requests
from bs4 import BeautifulSoup
from config import settings

def get_openphish_feed():
    url = settings.OPENPHISH_URL

    response = requests.get(url, timeout=10)
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, "html.parser")
        table = soup.find("table")
        rows = table.find_all("tr")
        feeds = []
        for row in rows[1:]:
            cells = row.find_all("td")
            if len(cells) == 3:
                url = cells[0].text.strip()
                tar_brand = cells[1].text.strip()
                time = cells[2].text.strip()
                feed = {"url": url, "target_brand": tar_brand, "time": time}
                feeds.append(feed)
        return feeds
    else:
        print(f"Error while fetching OpenPhish feed. Status code: {response.status_code}")
        return []
