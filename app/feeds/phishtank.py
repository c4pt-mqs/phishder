import requests
from config import settings

url = settings.PHISHTANK_URL

def get_phishtank_feed():
    response = requests.get(url, timeout=10)
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        print(f"Error while fetching Phishtank feed. Status code: {response.status_code}")
        return []
